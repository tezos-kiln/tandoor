# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA
{
  nixConfig = {
    flake-registry = "https://github.com/serokell/flake-registry/raw/master/flake-registry.json";
  };

  inputs = {
    flake-compat = {
      flake = false;
    };
  };

  outputs = { self, nixpkgs, serokell-nix, flake-compat }:

    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux.extend
          (nixpkgs.lib.composeManyExtensions [ serokell-nix.overlay ]);

      lib = pkgs.lib;

    in {
      # nixpkgs revision pinned by this flake
      legacyPackages.x86_64-linux = pkgs;

      # derivations that we can run from CI
      checks.x86_64-linux = {
        trailing-whitespace = pkgs.build.checkTrailingWhitespace ./.;
        reuse-lint = pkgs.build.reuseLint ./.;
      };
    };
}
