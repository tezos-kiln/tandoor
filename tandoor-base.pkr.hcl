# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

packer {
  required_plugins {
    virtualbox = {
      version = ">= 1.0.1"
      source  = "github.com/hashicorp/virtualbox"
    }
  }
}

variable "iso_url" {
  type = string
}

variable "iso_checksum" {
  type = string
}

variable "memory" {
  type = string
}

variable "cpus" {
  type = string
}

variable "disk_size" {
  type = string
}

variable "headless" {
  type = bool
}

source "virtualbox-iso" "debian" {
  iso_url = var.iso_url
  iso_checksum = var.iso_checksum
  boot_wait = "5s"
  boot_command = [
"<esc> auto preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<enter>"
  ]
  ssh_username = "baker"
  ssh_password = "baker"
  ssh_port = 22
  ssh_pty = true
  guest_os_type = "Debian_64"
  ssh_wait_timeout = "10000s"
  memory = var.memory
  cpus = var.cpus
  shutdown_command = "echo 'tandoor' | sudo -S shutdown -P now"
  http_directory = "http"
  format = "ova"
  headless = var.headless
  hard_drive_interface = "sata"
  vm_name = "debian"
  output_directory = "artifacts/debian/"
  disk_size = var.disk_size
  vboxmanage = [
    # Disable firewall to allow installer fetch preseed.cfg file
    ["modifyvm", "{{.Name}}", "--nat-localhostreachable1", "on"],
  ]
}

build {
  name = "tandoor-base"
  sources = ["sources.virtualbox-iso.debian"]
}

source "virtualbox-ovf" "debian" {
  source_path = "artifacts/debian/debian.ova"
  ssh_username = "baker"
  ssh_password = "baker"
  ssh_port = 22
  ssh_pty = true
  ssh_wait_timeout = "10000s"
  shutdown_command = "echo 'tandoor' | sudo -S shutdown -P now"
  http_directory = "http"
  format = "ova"
  headless = true
  vm_name = "tandoor-base"
  guest_additions_mode = "attach"
  output_directory = "artifacts/tandoor-base/"
}

build {
  name = "tandoor-base"
  sources = ["sources.virtualbox-ovf.debian"]
  provisioner "shell" {
    execute_command = "{{.Vars}} sudo -S -E sh -eux '{{.Path}}'"
    script = "./scripts/initial-setup/apt-configure.sh"
  }
  provisioner "shell" {
    execute_command = "{{.Vars}} sudo -S -E sh -eux '{{.Path}}'"
    environment_vars = [
      "SSH_USER=baker",
      "DEBIAN_FRONTEND=noninteractive",
      "DEBCONF_NONINTERACTIVE_SEEN=true",
    ]
    scripts = [
      "./scripts/initial-setup/install-nodejs.sh",
      "./scripts/initial-setup/update-grub.sh",
      "./scripts/initial-setup/network-configure.sh",
      "./scripts/initial-setup/install-gui.sh",
      "./scripts/initial-setup/install-guest-additions.sh",
      "./scripts/initial-setup/cleanup.sh",
    ]
  }
}
