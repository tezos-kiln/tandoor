#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

apt install -y dkms "linux-headers-$(uname -r)"

# Note: in tandoor the time sync feature of guest addition will be disabled in
# favor of NTP, see 'tandoor.pkr.hcl' and 'setup-ntp.sh'
mkdir /tmp/vboxguestadditions
mount /dev/cdrom /tmp/vboxguestadditions
sh /tmp/vboxguestadditions/VBoxLinuxAdditions.run || true
umount /tmp/vboxguestadditions
