#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

# Set up NTP client to sync time, instead of relying on VBox's Guest Additions.
# NOTE: to prevent conflicts it's also necessary to forbid Guest Additions from
# accessing the host time, see 'tandoor.pkr.hcl'.

# 'systemd-timesyncd' is enabled already, but its 'ConditionVirtualization'
# option by default is set to '!container', which prevents it from starting if
# it detects that it's running in a VM.
# Here we use a drop-in file to change that option.
mkdir -p /etc/systemd/system/systemd-timesyncd.service.d/
echo \"[Unit]\nConditionVirtualization=\" > /etc/systemd/system/systemd-timesyncd.service.d/allow_on_vms.conf

# 'systemd-timesyncd' is also not allowed to launch by the 'vboxadd-service'
# service, which lists it as a conflict.
# Here 'vboxadd-service' is edited in full because 'Conflicts' in systemd can't
# be overridden using drop-in files.
SYSTEMD_EDITOR="sed -i s/Conflicts=.*/Conflicts=shutdown.target/g" systemctl edit --full vboxadd-service.service

# Finally, start 'systemd-timesyncd' and wait for the system time to sync
systemctl start systemd-timesyncd
while [[ $(timedatectl status | grep 'System clock synchronized' | grep -Eo '(yes|no)') == no ]]; do
    sleep 1
done
