#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

cat <<EOF > /etc/network/interfaces
source /etc/network/interfaces.d/*

auto lo
iface lo inet loopback
EOF

echo 'tandoor' > /etc/hostname
echo '127.0.0.1 tandoor' >> /etc/hosts

apt install -y net-tools network-manager
systemctl enable NetworkManager.service --now
