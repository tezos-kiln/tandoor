#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

# These lines are appended to 'profiles.ini' file to set
# the default profile to 'baker'.
#
# The hexademical suffix is calculated from the installation
# path and will not change.
profiles_ini_install_info=$(cat << 'EOF'
[Install3B6073811A6ABF12]
Default=baker
Locked=1
EOF
)

# The contents of 'user.js' file which serves to override
# the preferences for a given profile. The essence of the
# following preferences is as follows:
#
# * allow override the homepage
# * skip opening the welcome page instead of homepage on
#   the first run of the browser
# * set the given homepage url
user_js_contents=$(cat << 'EOF'
user_pref("browser.startup.homepage_override.mstone", "ignore");
user_pref("trailhead.firstrun.didSeeAboutWelcome", true);
user_pref("browser.startup.homepage", "https://tezos-kiln.gitlab.io/tandoor-docs");
EOF
)

firefox_path="/home/baker/.mozilla/firefox"
baker_profile_path="$firefox_path/baker"

# Create the 'baker' profile
# NB 1: we run 'firefox' command as 'baker' user since it
# can't be run as root
#
# NB 2: we set the 'DISPLAY' environment variable to ':0'
# (primary monitor) since this variable is required to
# be set by 'firefox' command
DISPLAY=:0 sudo -u baker firefox -CreateProfile "baker $baker_profile_path"

# Mimic Firefox behavior and manually create 2 standard profiles
# to prevent 'profiles.ini' file be automatically recreated on
# the first browser run.
DISPLAY=:0 sudo -u baker firefox -CreateProfile default
DISPLAY=:0 sudo -u baker firefox -CreateProfile default-esr

# Edit the 'profiles.ini' file to set the default profile
# to 'baker'.
echo "$profiles_ini_install_info" >> $firefox_path/profiles.ini

# Set the necessary preferences for the 'baker' profile.
echo "$user_js_contents" > $baker_profile_path/user.js

chown -R baker:baker "$firefox_path"
