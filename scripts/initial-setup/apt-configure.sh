#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

cat <<EOF >> /etc/apt/apt.conf
APT::Install-Recommends "0";
APT::Install-Suggests "0";
EOF
