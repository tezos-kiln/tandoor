#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

mv_regular_files () {
    mkdir -p "$2"
    mv "$1"/* "$2/"
    chown -R baker:baker "$2"
}

mv_executable_files () {
    mv_regular_files "$1" "$2"
    chmod 755 "$2"/*
}


files="/tmp/static"
HOME="/home/baker"


desktop="$HOME/Desktop"
mv_executable_files "$files/desktop" "$desktop"


autostart="$HOME/.config/autostart"
mv_executable_files "$files/autostart" "$autostart"


pictures="$HOME/Pictures"
mv_executable_files "$files/pictures" "$pictures"


xfconf_dir="$HOME/.config/xfce4/xfconf/xfce-perchannel-xml"
mv_regular_files "$files/xfce4/xfconf" "$xfconf_dir"


scripts="$HOME/scripts"
mv_executable_files "$files/scripts" "$scripts"


echo 'alias reboot="xfce4-session-logout --reboot"' >> "$HOME/.bashrc"


usermod -aG adm baker
usermod -aG systemd-journal baker


apt install -y bc x11-utils wmctrl xdotool vim libnotify-bin xfce4-notifyd
