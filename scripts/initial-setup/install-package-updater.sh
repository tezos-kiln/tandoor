#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

apt-get -y install package-update-indicator gnome-packagekit xmlstarlet gnome-package-updater

# Set package-update-indicator to refresh every two hours
mkdir -p /home/baker/.config/package-update-indicator/
cat <<EOF > /home/baker/.config/package-update-indicator/package-update-indicator.conf
[General]
refresh-interval=7200
EOF

# Update default packagekit policies to allow updates without root privileges
xmlstarlet edit -L \
  -u "/policyconfig/action[@id='org.freedesktop.packagekit.system-update']/defaults/allow_any" -v "yes" \
  -u "/policyconfig/action[@id='org.freedesktop.packagekit.system-update']/defaults/allow_inactive" -v "yes" \
  -u "/policyconfig/action[@id='org.freedesktop.packagekit.system-update']/defaults/allow_active" -v "yes" \
  /usr/share/polkit-1/actions/org.freedesktop.packagekit.policy

# Remove 'xmlstarlet', as it's unlikely to be useful to end users
apt-get -y remove xmlstarlet
