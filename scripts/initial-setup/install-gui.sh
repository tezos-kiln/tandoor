#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

# install xserver
apt install -y xserver-xorg-core xserver-xorg xfonts-base xinit

# install desktop environment
apt install -y xfce4 xfce4-terminal at-spi2-core tango-icon-theme

# install web browser
apt install -y firefox-esr

# install login manager
apt install -y lightdm lightdm-gtk-greeter accountsservice

echo '/usr/sbin/lightdm' > /etc/X11/default-display-manager
dpkg-reconfigure lightdm
echo set shared/default-x-display-manager lightdm | debconf-communicate

cat <<EOF > /etc/lightdm/lightdm.conf
[Seat:*]
greeter-session=lightdm-gtk-greeter
autologin-user=baker
autologin-user-timeout=0
autologin-session=xfce
EOF

# install log viewer
apt install -y ksystemlog rsyslog
