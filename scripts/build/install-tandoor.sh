#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

set -euo pipefail

timestamp="$(ls -r artifacts | grep 'tandoor-[0-9]' | head -1 | cut -d- -f2)"
VBoxManage import "artifacts/tandoor-$timestamp/tandoor-$timestamp.ova"
