#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

set -euo pipefail

mkdir -p artifacts

if [ "${REBUILD_FIRST_STAGE-false}" = "true" ]; then
    packer init tandoor-base.pkr.hcl
    # builds fresh debian image
    packer build -force -var-file tandoor-base.pkrvars.hcl -only=tandoor-base.virtualbox-iso.debian tandoor-base.pkr.hcl
    # provisions debian image to be used as a base for tandoor
    packer build -force -var-file tandoor-base.pkrvars.hcl -only=tandoor-base.virtualbox-ovf.debian tandoor-base.pkr.hcl
else
    mkdir -p artifacts/tandoor-base
    curl -L "$TANDOOR_BASE_URL" -o artifacts/tandoor-base/tandoor-base.ova
fi
