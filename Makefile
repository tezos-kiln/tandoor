# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

.PHONY: all install uninstall clean cleancache force-base-build

tandoor-base := artifacts/tandoor-base/tandoor-base.ova

tandoor := artifacts/.tandoor

TANDOOR_BASE_URL ?= https://gitlab.com/tezos-kiln/tandoor/-/package_files/106483368/download

# Iff the REBUILD_FIRST_STAGE env var is set to true this resolves to `force-base-build`
# This can be used as a sort of "conditional target".
rebuild-base = $(if $(filter "$(REBUILD_FIRST_STAGE)","true"), force-base-build)

all: $(tandoor)

# PHONY target used only to force (re)builds of the `$(tandoor-base)` target.
# See also the comment on `rebuild-base`.
# Because this is never satisified, when used as a dependency of a target, the
# latter is forced to be (re)built.
force-base-build:
	$(info NOTE: the rebuild of the first stage was forced)

$(tandoor-base): tandoor-base.pkr.hcl tandoor-base.pkrvars.hcl $(wildcard http/*) $(rebuild-base)
	TANDOOR_BASE_URL=${TANDOOR_BASE_URL} REBUILD_FIRST_STAGE=${REBUILD_FIRST_STAGE} ./scripts/build/get-tandoor-base.sh

$(tandoor): $(tandoor-base) tandoor.pkrvars.hcl tandoor.pkr.hcl
	./scripts/build/build-tandoor.sh

install: $(tandoor)
	./scripts/build/install-tandoor.sh

clean: uninstall
	rm -rf artifacts $(tandoor)

uninstall:
	./scripts/build/uninstall-tandoor.sh
