#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

import os
import time
from subprocess import run, check_call

# This launches the setup wizard if there isn't a previous setup already

def get_node_state():
    return run("sudo systemctl is-active tezos-node-*.service", capture_output=True, shell=True)

def is_active(node_state):
    return "active" in node_state.stdout.decode().splitlines()

def is_activating(node_state):
    return "activating" in node_state.stdout.decode().splitlines()

def notify(message):
    check_call(f"notify-send 'Tandoor' '{message}' --icon=dialog-information", shell=True)

try:
    # we first look for any node to be running (either alone or with baking)
    node_state = get_node_state()
    # sometimes (especially with tezos-baking services) a node unit is still in
    # the process of being activated at this point
    # if there is an active or activating node, the setup wizard isn't launched
    if is_active(node_state) or is_activating(node_state):
        exit()
    # otherwise a maximize terminal will be launched
    check_call('xfce4-terminal --maximize -e "return_to_exit tezos-setup" && /home/baker/scripts/open-apps.py', shell=True)

except Exception as e:
    print(e)
