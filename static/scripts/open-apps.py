#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

import os
import time
from subprocess import run, check_call

def get_node_state():
    return run("sudo systemctl is-active tezos-node-*.service", capture_output=True, shell=True)

def is_active(node_state):
    return "active" in node_state.stdout.decode().splitlines()

def is_activating(node_state):
    return "activating" in node_state.stdout.decode().splitlines()

def notify(message):
    check_call(f"notify-send 'Tandoor' '{message}' --icon=dialog-information", shell=True)

def window_opened(window_list, title):
    for window in window_list:
        if title in window:
            return True
    return False

def open_apps():
    check_call("firefox &", shell=True)
    check_call("firefox --new-window http://localhost:2020 &", shell=True)
    check_call("xfce4-terminal -e \"journalctl -f _UID=$(id -u tezos)\" --title=tezos_baking_logs &", shell=True)

    while True:
        window_list = run("wmctrl -l", shell=True, capture_output=True).stdout.decode("utf-8").split("\n")
        if window_opened(window_list, "Pyrometer — Mozilla Firefox") and window_opened(window_list, "tezos_baking_logs") and window_opened(window_list, "Mozilla Firefox"):
            break
        time.sleep(1)

# it can happen that state is `failed`, but node handles it by itself
for _ in range(5):
    node_state = get_node_state()
    if is_active(node_state) or is_activating(node_state):
        break
    time.sleep(2)

node_state = get_node_state()
node_active = is_active(node_state)
if node_active or is_activating(node_state):
    notify("Waiting for the node service to start...")
    while not node_active:
        node_active = is_active(get_node_state())
        time.sleep(2)

    notify("Node service started")

    notify("Waiting for the apps to open...")
    open_apps()
    check_call("/home/baker/scripts/tile.sh", shell=True)
    notify("Apps are opened.")
