#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

function get_workspace {
  if [[ "$DTOP" == "" ]] ; then
    DTOP=`xdotool get_desktop`
  fi
}

function is_desktop {
  xwininfo -id "$*" | grep '"Desktop"'
  return "$?"
}

function get_visible_window_ids {
  if (( ${#WDOWS[@]} == 0 )) ; then
    WDOWS=(`xdotool search --desktop $DTOP --onlyvisible "" 2>/dev/null`)
  fi
}

function get_desktop_dim {
  if (( ${#DIM[@]} == 0 )) ; then
    DIM=(`wmctrl -d | egrep "^0" | sed 's/.*DG: \([0-9]*x[0-9]*\).*/\1/g' | sed 's/x/ /g'`)
  fi
}

function win_tile {
  get_workspace
  get_visible_window_ids

  (( ${#WDOWS[@]} < 1 )) && return;

  get_desktop_dim

  cols=`echo "sqrt(${#WDOWS[@]})" | bc`
  rows=$cols
  wins=`expr $rows \* $cols`

  if (( "$wins" < "${#WDOWS[@]}" )) ; then
    cols=`expr $cols + 1`
    wins=`expr $rows \* $cols`
    if (( "$wins" < "${#WDOWS[@]}" )) ; then
      rows=`expr $rows + 1`
      wins=`expr $rows \* $cols`
    fi
  fi

  (( $cols < 1 )) && cols=1;
  (( $rows < 1 )) && rows=1;

  win_w=`expr ${DIM[0]} / $cols`
  win_h=`expr ${DIM[1]} / $rows`

  # do tiling
  x=0; y=0; commands=""
  for window in "${WDOWS[@]}" ; do
    wmctrl -i -r $window -b remove,maximized_vert,maximized_horz

    commands="$commands windowsize $window $win_w $win_h"
    commands="$commands windowmove $window `expr $x \* $win_w` `expr $y \* $win_h`"

    x=`expr $x + 1`
    if (( $x > `expr $cols - 1` )) ; then
      x=0
      y=`expr $y + 1`
    fi
  done

  echo "$commands" | xdotool -
}

win_tile
