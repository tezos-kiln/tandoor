# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

source_path = "artifacts/tandoor-base/tandoor-base.ova"

version = "{{timestamp}}"

tandoor_branch = "master"
