# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

packer {
  required_plugins {
    virtualbox = {
      version = ">= 1.0.1"
      source  = "github.com/hashicorp/virtualbox"
    }
  }
}

variable "source_path" {
  type = string
}

variable "version" {
  type = string
}

variable "tandoor_branch" {
  type = string
}

source "virtualbox-ovf" "tandoor" {
  source_path = "${var.source_path}"
  vm_name = "tandoor-${var.version}"
  ssh_username = "baker"
  ssh_password = "baker"
  ssh_port = 22
  ssh_pty = true
  ssh_wait_timeout = "10000s"
  shutdown_command = "tezos-client --version | sudo -S shutdown -P now"
  http_directory = "http"
  format = "ova"
  # since only provisioning is made, all logs will be shown in the terminal window
  headless = true
  output_directory = "artifacts/tandoor-${var.version}/"
  guest_additions_mode = "attach"
  vboxmanage = [
    # Enable usb and add a filter for Ledger Nano S device
    ["modifyvm", "{{.Name}}", "--usb", "on"],
    ["modifyvm", "{{.Name}}", "--usbehci", "on"],
    ["usbfilter", "add", "0", "--target", "{{.Name}}", "--name", "Ledger", "--vendorid", "0x2c97", "--productid", "0x0001"],
    # Add port forwarding for default pyrometer port
    ["modifyvm", "{{.Name}}", "--natpf1", "Pyrometer,tcp,,2020,,2020"],
    # Add ssh port forwarding
    ["modifyvm", "{{.Name}}", "--natpf1", "ssh,tcp,,2222,,22"],
    # Minimum recommended size
    ["modifyvm", "{{.Name}}", "--vram", "16", "--graphicscontroller", "vmsvga"],
    # Prevent access to the host's time, see: https://docs.oracle.com/en/virtualization/virtualbox/7.0/user/AdvancedTopics.html#disabletimesync
    ["setextradata", "{{.Name}}", "VBoxInternal/Devices/VMMDev/0/Config/GetHostTimeDisabled", "1"],
    # Enable bidirectional clipboard
    ["modifyvm", "{{.Name}}", "--clipboard-mode", "bidirectional"],
  ]
}

build {
  name = "tandoor"
  sources = ["sources.virtualbox-ovf.tandoor"]
  provisioner "file" {
    source = "static"
    destination = "/tmp/static"
  }
  provisioner "shell" {
    execute_command = "{{.Vars}} sudo -S -E bash -eux '{{.Path}}'"
    scripts = [
      "./scripts/initial-setup/setup-ntp.sh",
    ]
  }
  provisioner "shell" {
    execute_command = "{{.Vars}} sudo -S -E sh -eux '{{.Path}}'"
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]
    inline = ["apt -y update && apt -y upgrade"]
  }
  provisioner "shell" {
    execute_command = "{{.Vars}} sudo -S -E sh -eux '{{.Path}}'"
    environment_vars = [
      "TANDOOR_BRANCH=${var.tandoor_branch}",
      "DEBIAN_FRONTEND=noninteractive",
    ]
    scripts = [
      "./scripts/initial-setup/install-tezos-baking.sh",
      "./scripts/initial-setup/setup-gui.sh",
      "./scripts/maintenance/install-latest-pyrometer.sh",
      "./scripts/initial-setup/setup-pyrometer.sh",
      "./scripts/initial-setup/install-package-updater.sh",
      "./scripts/initial-setup/install-maintenance-scripts.sh",
      "./scripts/initial-setup/set-firefox-homepage.sh",
      "./scripts/initial-setup/cleanup.sh",
    ]
  }
  provisioner "shell" {
    execute_command = "{{.Vars}} sudo -S -E sh -eux '{{.Path}}'"
    inline = [
      # configure journald for correct log viewing
      "printf Storage=persistent\n >> /etc/systemd/journald.conf",
      # Add cron job for pyrometer update
      "sudo bash -c 'crontab -l | { cat; echo \"0 */4 * * * /usr/local/bin/install-latest-pyrometer\"; } | crontab -'",
      # Scripts from ./scripts/
      "sudo bash -c 'crontab -l | { cat; echo \"0 */4 * * * git -C /opt/tandoor pull --rebase\"; } | crontab -'",
    ]
  }
}
